import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton supagettiButton;
    private JButton gyozaButton;
    private JTextPane ordersList;
    private JButton checkOutButton;
    private JLabel price;
    private JLabel priceZ;
    private JLabel yen;

    int priceA=-0;
    void order (String food,int itemPrice){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to oder "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0) {

            int confirmation1 = JOptionPane.showConfirmDialog(null,
                    "Would you like to order Omori(+100yen)?",
                    "Select Omori",
                    JOptionPane.YES_NO_OPTION);
            if(confirmation1 == 0) {
                JOptionPane.showMessageDialog(null, "OK! You choosed Omori version.");
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
                String currentText = ordersList.getText();
                itemPrice += 100;
                ordersList.setText(currentText + food +"（大盛り） "+String.valueOf(itemPrice)+ "yen(税込み:" +String.valueOf((int)(itemPrice*1.1))+"yen)\n");

                int number = Integer.parseInt(price.getText());
                priceA = number + itemPrice;
                price.setText(String.valueOf(priceA));
                priceZ.setText(String.valueOf((int)(priceA*1.1)));
            }
            else{
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
                String currentText = ordersList.getText();
                ordersList.setText(currentText + food +"（並盛り） "+String.valueOf(itemPrice)+ "yen(税込み:" +String.valueOf((int)(itemPrice*1.1))+"yen)\n");

                int number2 = Integer.parseInt(price.getText());
                priceA = number2 + itemPrice;
                price.setText(String.valueOf(priceA));
                priceZ.setText(String.valueOf((int)(priceA*1.1)));
            }
        }
    }
    void checkout (){
        int confirmationA = JOptionPane.showConfirmDialog(null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmationA == 0) {
            JOptionPane.showMessageDialog(null, "Thank you! The total price is " + priceA + "yen.");
            ordersList.setText("");
            priceA = 0;
            price.setText(String.valueOf(priceA));
        }
    }

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("Tempura",100);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",300);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",300);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",200);
            }
        });
        supagettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Supagetti",300);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",200);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
